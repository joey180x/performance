package com.josephreddit.performance;

import android.support.v4.app.Fragment;
import android.os.Bundle;

import java.util.ArrayList;

public class MainActivity extends SingleFragmentActivity {

    public ArrayList<ToDoPost> todoItems = new ArrayList<ToDoPost>();

    @Override
    protected Fragment createFragment() {
        return new ToDoListFragment();
    }

    @Override
    protected int getLayoutRedId() {
        return R.layout.activity_fragment;    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


}
